# NO TEARS on Argoverse 2

Running NO TEARS on Argoverse 2.

## Installation

If you have poetry installed:

```
$ poetry install
```

Otherwise, install all the dependencies listed on
[pyproject.toml](pyproject.toml).

## Usage

To execute the program:

```
$ poetry run python main.py --dataset_tar av2.tar --scenario_n 10 --closest_object_n 4
```

Where `av2.tar` is a tar archive with Argoverse 2 Motion
Forecasting data. As of the writing of this document, you can download the data
from [argoverse.org](https://www.argoverse.org/av2.html).

Hint: You can also provide your own Tar with a subset of the data.

Execute with `-h` to see description of the other parameters.

## Output

A file named `model.svg` (or the name specified with `--output_fig`) like the one below:

![Causal model](model.svg)

## Experiment details

We construct a list of vectors corresponding to the closest objects to the
ego-vehicle at each time-step (including the ego-vehicle). The values in the
vector are raw values associated with each corresponding object (i.e., no
normalization of any type is applied).
