"""Execute NO TEARS on Argoverse 2 Motion Forecasting data."""
from notears.linear import notears_linear
from scenario import defaultdict, scenario_to_variables
from scenario import extract_scenarios
from pathlib import Path
import matplotlib.pyplot as plt
import argparse
import torch


def main():
    parser = argparse.ArgumentParser(
        description='Execute NO TEARS on Argoverse 2 Motion Forecasting data.'
    )
    parser.add_argument('--dataset_tar', type=Path, required=True)
    parser.add_argument(
        '--output_fig',
        default="model.svg",
        type=Path,
        required=False,
    )
    parser.add_argument(
        '--scenario_n',
        help="Number of scenarios to extract from the dataset.",
        type=int,
        required=True
    )
    parser.add_argument(
        '--closest_object_n',
        help="Number of objects to include in the vectorization of each time-step.",
        type=int,
        required=True
    )
    parser.add_argument(
        '--seed',
        help="Seed used to choose scenarios from the dataset.",
        type=str,
        required=False,
        default="asdf",
    )
    args = parser.parse_args()

    # Extract scenarios
    scenarios = extract_scenarios(
        args.dataset_tar,
        k=args.scenario_n,
        seed=args.seed,
    )

    # Convert the scenarios to (variables -> list of values)
    variables = defaultdict[str, list[float]](list)
    for scenario in scenarios:
        scenario_variables = scenario_to_variables(
            scenario,
            args.closest_object_n
        )
        for variable, values in scenario_variables.items():
            variables[variable].extend(values)
    variable_names = sorted(variables.keys())

    # Convert variables to array
    X = torch.tensor([variables[v] for v in variable_names]).numpy().T

    # Execute linear NO TEARS
    # Hyperparameters from:
    # https://github.com/xunzheng/notears/blob/master/notears/linear.py
    W_est = notears_linear(X, lambda1=0.1, loss_type='l2')

    # Plot heatmap
    fig, ax = plt.subplots()
    im = ax.imshow(W_est, cmap='bwr')
    ax.set_xticks(range(len(variable_names)), labels=variable_names)
    ax.set_yticks(range(len(variable_names)), labels=variable_names)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.figure.colorbar(im, ax=ax)
    ax.set_title(f"Linear NO TEARS ({len(scenarios)} scenarios; {len(X)} observations)")
    fig.tight_layout()

    # Write heatmap
    fig.savefig(args.output_fig)
    print(f"Wrote {args.output_fig}")

if __name__ == "__main__":
    main()
