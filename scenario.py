"""Type definition for an Argoverse 2 motion forecasting scenario."""
from collections import defaultdict
from pathlib import Path
import tempfile
import tarfile
import torch
import random
from av2.datasets.motion_forecasting import scenario_serialization
from av2.datasets.motion_forecasting.data_schema import ArgoverseScenario


def extract_scenarios(
        argoverse_tar: Path,
        k: int,
        seed: str,
        ) -> list[ArgoverseScenario]:
    """Return a list of `k` scenarios in the given argoverse archive.

    The scenarios are chosen randomly using the given `seed`.
    """
    # Create list of scenarios
    scenarios = list()

    # Create temporary directory
    with tempfile.TemporaryDirectory() as extracted_dataset_dir:
        # Extract dataset to temporary directory
        with tarfile.open(argoverse_tar, 'r') as tar_ref:
            # Identify the files in the archive that are .parquet files
            members = [
                m for m in tar_ref.getmembers()
                if "parquet" in m.name
            ]

            # Choose a subset of the dataset to extract
            _random = random.Random(seed)
            chosen_members = _random.sample(members, k=k)
            tar_ref.extractall(
                extracted_dataset_dir,
                members=chosen_members,
            )

        # Find scenario files
        scenario_files = list(
            Path(extracted_dataset_dir).glob("**/*.parquet")
        )

        # Decompress scenario files
        for scenario_file in scenario_files:
            scenario = scenario_serialization.load_argoverse_scenario_parquet(
                scenario_file
            )
            scenarios.append(scenario)

    return scenarios


def get_egovehicle_positions(
        scenario: ArgoverseScenario,
        ) -> dict[int, tuple[float, float]]:
    """Return a dictionary mapping time-steps to ego-vehicle coordinates."""
    # Obtain ego-vehicle track
    ego_tracks = list(filter(
        lambda track: track.track_id == scenario.focal_track_id,
        scenario.tracks
    ))
    assert len(ego_tracks) == 1, "There should only be one focal track!"
    track = ego_tracks[0]

    # Obtain the ego-vehicle position at each time-step
    positions = dict()
    for state in track.object_states:
        t = state.timestep
        positions[t] = state.position
    return positions


def distance(p1: tuple[float, float], p2: tuple[float, float]) -> float:
    return (torch.tensor(p1)-torch.tensor(p2)).norm()


def get_closest_observed_objects(
        scenario: ArgoverseScenario,
        position: tuple[float, float],
        timestep: int,
        k: int,
        ) -> list[scenario_serialization.ObjectState]:
    """Return the `k` closest objects to the given position at the given
    timestep. The objects are sorted in increasing distance to `position`.

    If there are less than `k` objects associated with `timestep`, an exception
    is raised.
    """
    objects = [
        state
        for track in scenario.tracks
        for state in track.object_states
        if state.timestep == timestep
    ]
    if len(objects) < k:
        raise ValueError(f"Cannot get {k} objects at timestep {timestep} with only {len(objects)} objects!")
    return sorted(objects, key=lambda o: distance(o.position, position))[:k]


def get_vectorized_object(
        o: scenario_serialization.ObjectState
        ) -> dict[str, float]:
    """Return a dictionary mapping variable names to numeric values
    representing the given object."""
    return dict(
        x=o.position[0],
        y=o.position[1],
        vx=o.velocity[0],
        vy=o.velocity[1],
    )


def scenario_to_variables(
        scenario: ArgoverseScenario,
        k: int
        ) -> dict[str, list[float]]:
    """Vectorize each time-step in the scenario into a series of named
    variables. The returned dictionary specifies the value of each variable
    at each time-step.

    The parameter `k` indicates the number of objects that will be used to
    vectorize each time-step. Objects are chosen according to their distance
    to the ego-vehicle.

    Any time-step with less than `k` objects is ignored.
    """
    variables = defaultdict(list)

    # Get ego-vehicle positions
    positions = get_egovehicle_positions(scenario)

    # Vectorize the closest objects to the ego-vehicle at each time-step
    for t in sorted(positions.keys()):
        position = positions[t]
        try:
            objects = get_closest_observed_objects(
                scenario=scenario,
                position=position,
                timestep=t,
                k=k,
            )
        except ValueError:
            # Ignore time-step with less than k objects
            continue
        # Vectorize each object
        for i, o in enumerate(objects):
            vectorized_object = get_vectorized_object(o)
            for variable, value in vectorized_object.items():
                # Rename the variable to specify the object it refers to
                new_variable = f"obj{i}_{variable}"
                variables[new_variable].append(value)

    variable_lens = [len(values) for values in variables.values()]
    assert len(set(variable_lens)) == 1, f"Scenario variables have differing lengths {variable_lens}!"

    return variables
